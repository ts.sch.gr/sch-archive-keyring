# sch-archive-keyring

Add the ts.sch.gr repositories and keys to apt sources:

 * Educational software repository (ts.sch.gr/repo)
 * School updates repository (ppa:ts.sch.gr)
 * Proposed school updates repository (ppa:ts.sch.gr/proposed)

Only LTS Ubuntu releases can use the PPAs.
All the other releases, including Debian, can only use the educational software repository.

## Technical notes

The package name needs to [end in -archive-keyring](https://lintian.debian.org/tags/package-installs-apt-keyring.html).
The following commands were used to create the keyring.
The first two keys are deprecated.

```shell
# Add all five keys with commands like the following
$ gpg --no-default-keyring --primary-keyring ./sch-archive-keyring.gpg \
--keyserver keyserver.ubuntu.com --recv-keys 4F3A3E9FC07F4E831D87AC2D3C72607B03AFA832

# List the keyring contents
$ gpg --no-default-keyring --primary-keyring ./sch-archive-keyring.gpg --list-keys
./sch-archive-keyring.gpg
-------------------------
pub   rsa1024 2009-05-27 [SC]
      4F3A3E9FC07F4E831D87AC2D3C72607B03AFA832
uid           [ unknown] Αποθετήριο Τεχνικής Στήριξης ΣΕΠΕΗΥ

pub   dsa1024 2009-05-27 [SC]
      B2239C980BD033673A0F329049752CD09849BE20
uid           [ unknown] Hellenic Schools Technical Support Team (Τεχνική Στήριξη ΣΕΠΕΗΥ) <devs@ts.sch.gr>
sub   elg2048 2009-05-27 [E]

pub   rsa4096 2020-04-16 [SC]
      DA9F0815868A08FC0CF4CB13F96858B750856CB9
uid           [ unknown] Hellenic Schools Technical Support Team <devs@ts.sch.gr>
sub   rsa4096 2020-04-16 [E]

pub   rsa1024 2011-10-05 [SC]
      2D9B9274D3DF9D7CD7281A3E49F26CB30350B375
uid           [ unknown] Launchpad PPA for Epoptes Developers

pub   rsa4096 2019-08-10 [SC]
      8A3246F8D6CF2B67C979C987B64988E8F9B7EF68
uid           [ unknown] Launchpad PPA for LTSP
